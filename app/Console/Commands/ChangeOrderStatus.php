<?php

namespace App\Console\Commands;

use App\Models\Orders;
use Illuminate\Console\Command;

class ChangeOrderStatus extends Command
{
    protected $signature = 'Order:change';

    public function handle()
    {
        $currentTime = time();
        Orders::where('time', "<", $currentTime)
            ->where('status', 1)->update(['status' => 4]);
    }
}
