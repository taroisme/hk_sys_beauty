<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\GuestBook;
use Illuminate\Http\Request;

class GuestBookController extends Controller
{
    public function get(Request $request)
    {
        $data = $request->all();
        $per_page = isset($data['limit']) ? $data['limit'] : 15;
        $gbs = GuestBook::query();
        if ($data['name']) {
            $gbs->where("name", 'LIKE', "%{$data['name']}%");
        }
        if ($data['email']) {
            $gbs->where("emial", 'LIKE', "%{$data['email']}%");
        }
        if ($data['phone']) {
            $gbs->where("phone", 'LIKE', "%{$data['phone']}%");
        }
        if ($data['status']) {
            $gbs->where("status", $data['status']);
        }
        $gbs =  $gbs->paginate($per_page);
        return $this->request_success_json($gbs);
    }

    public function update($gb_id)
    {
        GuestBook::where("id", $gb_id)->update(['status' => 2]);
        return $this->request_success_json("Updated successfully");
    }

    public function delete($gb_id)
    {
        GuestBook::where("id", $gb_id)->delete();
        return $this->request_success_json("Deleted successfully");
    }
}
