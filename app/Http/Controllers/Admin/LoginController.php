<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /**
     * 登录
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function login(Request $request)
    {
        // $this->validateLogin($request);
        $data = $request->all();
        $account = $data['account'];
        $password = $data['password'];
        if ($account == null || $password == null) {
            return $this->request_failed_json('賬號或者密碼不允許為空');
        }
        $user = User::where('telphone', $account)->orWhere('account', $account);
        if (!$user->exists()) {
            return $this->request_failed_json('賬號不存在');
        }

        if ($user->value('status') == 2) {
            return $this->request_failed_json('賬戶被凍結');
        }
        if (Hash::check($password, $user->value('password'))) {
            // $app_token = $this->generateToken();
            $acc = User::where('telphone', $account)->orWhere('account', $account)->first();
            return $this->request_success_json($acc);
        } else {
            return $this->request_failed_json('密碼錯誤');
        }
    }
}
