<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Clients;
use App\Models\OperationLog;
use App\Models\Orders;
use App\Models\Programs;
use App\Models\User;
use App\Models\WorkingArr;
use Facade\FlareClient\Http\Client;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * 工单列表
     */
    public function get(Request $request)
    {
        $data = $request->all();
        $per_page = isset($data['limit']) ? $data['limit'] : 15;
        $orders = Orders::query();
        if ($data['program_id']) {
            $orders->where("program_id", $data['program_id']);
        }
        if ($data['status']) {
            $orders->where("status", $data['status']);
        }
        if ($data['user_id']) {
            $orders->where("user_id", $data['user_id']);
        }
        if ($data['telphone']) {
            $client_id = Clients::where('telphone', $data['telphone'])->value('id');
            $orders->where('client_id', $client_id);
        }
        if ($data['name']) {
            $client_id = Clients::where('name', $data['name'])->value('id');
            $orders->where('client_id', $client_id);
        }
        if ($data['startAt'] && $data['endAt']) {
            $orders->whereBetween('time', [strtotime($data['startAt']), strtotime($data['endAt'])]);
        }
        if ($data['startAt'] && !$data['endAt']) {
            $orders->whereBetween('time', '>=', strtotime($data['startAt']));
        }
        if (!$data['startAt'] && $data['endAt']) {
            $orders->whereBetween('time', '<=', strtotime($data['endAt']));
        }
        $orders = $orders->orderByDesc('id')->paginate($per_page);
        if (count($orders) > 0) {
            foreach ($orders as $order) {
             
                $order['user'] = User::where('id', $order['user_id'])->value('name');
                $client = Clients::where('id', $order['client_id'])->first();
                $order['client'] = $client['name'];
                $order['isNewClient'] = $client['isNewClient'];
                $order['telphone'] = $client['telphone'];
                $order['time'] = date("Y-m-d H:i", $order['time']);
                $order['program'] = Programs::where('id', $order['program_id'])->value('name');
                $program_second = array();
                $ids = explode(",", $order['program_second_id']);
                foreach ($ids as $id) {
                    $program_second[] = Programs::where('id', $id)->value('name');
                }
                $order['program_second'] = $program_second;
                $logs = OperationLog::where("order_id", $order['id'])->get();
                foreach ($logs as $log) {
                    $log['client_name'] = Clients::where("id", $log['client_id'])->value("name");
                }
            }
        }
        return $this->request_success_json($orders);
    }

    /**
     * 管理员修改工单
     */
    public function update($order_id, Request $request)
    {
        $data = $request->all();
        $order =  Orders::where('id', $order_id);
        if ($data['status']) {
            $order->update(['status' => $data['status']]);
            if ($data['status'] == 2) {
                Clients::where('id', $order->value('client_id'))->update(['isNewClient' => 2]);
            }
        }
        if ($data['time']) {
            $user_id = WorkingArr::where('time', strtotime($data['time']))->value('user_id');
            $order->update(['time' => strtotime($data['time']), 'user_id' => $user_id]);
        }
        if ($data['program_id']) {
            $order->update(['program_id' => $data['program_id'], 'program_second_id' => $data['program_second_id']]);
        }
        if ($data['remarks']) {
            $order->update(['remarks' => $data['remarks']]);
        }
        // OperationLog::create([
        //     "user_id" => $data['user_id'],
        //     'order_id' => $order_id,
        //     'operation' => $data['remark']
        // ]);
        return $this->request_success_json("修改成功");
    }
}
