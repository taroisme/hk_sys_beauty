<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Orders;
use App\Models\Programs;
use App\Models\User;
use App\Models\WorkingArr;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
    /**
     * 项目列表
     */
    public function get(Request $request)
    {
        $data = $request->all();
        $programs = Programs::where("parent_id", 0);
        if ($data['status']) {
            $programs->where("status", $data['status']);
        }
        $programs = $programs->where("status", "<>", 3)->get();
        foreach ($programs as $program) {
            $sub_menus = Programs::where("parent_id", $program['id']);
            if ($data['status']) {
                $sub_menus->where("status", $data['status']);
            }
            $program["sub_menus"] = $sub_menus->where("status", "<>", 3)->get();
        }
        return $this->request_success_json($programs);
    }

    /**
     * 新建项目
     */
    public function create(Request $request)
    {
        $data = $request->all();
        $parent_id = 0;
        if ($data['level'] != 1) {
            $parent_id = $data['parent_id'];
        }
        $isExists = Programs::where("name", $data['name'])->where("status", 1)->exists();
        if ($isExists) {
            return $this->request_failed_json("Program already exists");
        }
        Programs::create([
            "name" => $data['name'],
            "parent_id" => $parent_id,
            "level" => $data["level"],
            "content" => $data['content']
        ]);
        return $this->request_success_json("Program create successfully");
    }

    /**
     * 修改项目
     */
    public function update($program_id, Request $request)
    {
        $data = $request->all();
        $program = Programs::where("name", $data['name'])->where("status", 1);
        if ($program->exists() && $program->value('id') != $program_id) {
            return $this->request_failed_json("Program already exists");
        }
        Programs::where("id", $program_id)->update([
            'name' => $data['name'],
            'status' => $data['status'],
            "content" => $data['content']
        ]);
        return $this->request_success_json("Program update successfully");
    }

    /**
     * 可预约时间-月
     */
    public function mTimeList(Request $request)
    {
        $data = $request->all();
        $datetime = $data['datetime'];
        $program_id = $data['program_id'];
        $days = $this->days($datetime);
        $arr = array();
        for ($i = 1; $i <= $days; $i++) {
            $today = array();
            $today["day"] = $i;
            $startAt = strtotime($datetime . "-" . $i . " 09:00");
            $endAt = strtotime($datetime . "-" . $i . " 23:00");
            $was = WorkingArr::whereBetween("time", [$startAt, $endAt])->where('program_id', $program_id);
            $time = array();
            foreach ($was->get() as $wa) {
                $time[] = date("H:i", $wa['time']);
            }
            $color = array();
            foreach ($was->select('user_id')->distinct()->get() as $wa) {
                $color[] = User::where('id', $wa['user_id'])->value('color');
            }
            $today['color'] = $color;
            $today['time'] = $time;
            $arr[] = $today;
        }
        return $this->request_success_json($arr);
    }


    /**
     * 可预约时间-日
     */
    public function dTimeList(Request $request)
    {
        $data = $request->all();
        $program_id = $data['program_id'];
        $startAt = $data['datetime'] . " 09:00";
        $endAt = $data['datetime'] . " 23:00";
        $timestamp = strtotime($endAt) - strtotime($startAt);
        $arr = array();
        for ($i = 0; $i <= $timestamp / 900; $i++) {
            $arrW = array();
            $time = strtotime($startAt) + 900 * $i;
            $wa = WorkingArr::where("time", $time)->where('program_id', $program_id);
            $arrW['time'] = date("Y-m-d H:i", $time);
            if ($wa->exists()) {
                $arrW['user_id'] = $wa->value("user_id");
                $arrW['wa_id'] = $wa->value("id");
                $arrW['user'] = User::where("id", $wa->value("user_id"))->value("name");
                $arrW['status'] = 2;
            } else {
                $arrW['status'] = 1;
            }
            $arr[] = $arrW;
        }
        return $this->request_success_json($arr);
    }


    /**
     * 创建工作计划
     */
    public function addWorkingArr(Request $request)
    {
        $data = $request->all();
        $time = strtotime($data['time']);
        WorkingArr::create([
            "time" => $time,
            "program_id" => $data['program_id'],
            "user_id" => $data['user_id']
        ]);
        $order = Orders::where('time', $time);
        if ($order->exists()) {
            $order->update(['user_id' => $data['user_id'], 'status' => 1]);
        }
        return $this->request_success_json("Create successfully");
    }

    /**
     * 删除工作计划
     */
    public function delWorkingArr($wa_id)
    {
        $wa = WorkingArr::where("id", $wa_id);
        $order = Orders::where('time',  $wa->value('time'))->where('program_id', $wa->value('program_id'));
        if ($order->exists()) {
            $order->update(['status' => 4]);
        }
        $wa->delete();

        return $this->request_success_json("Delete successfully");
    }
}
