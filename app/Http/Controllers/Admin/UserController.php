<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Clients;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * 用户列表
     */
    public function get(Request $request)
    {
        $data = $request->all();
        $per_page = isset($data['limit']) ? $data['limit'] : 15;
        $users = User::query();
        if ($data['status']) {
            $users->where('status', $data['status']);
        }
        $users =  $users->paginate($per_page);
        return $this->request_success_json($users);
    }

    /**
     * 用户创建
     */
    public function create(Request $request)
    {
        $data = $request->all();
        $isExists = User::where("telphone", $data['telphone'])->exists();
        if ($isExists) {
            return $this->request_failed_json("User already exists");
        }
        User::create([
            "telphone" => $data['telphone'],
            "password" => bcrypt($data['password']),
            "role" => 2,
            "name" => $data['name'],
            "color" => $data['color']
        ]);
        return $this->request_success_json("User created successfully");
    }

    /**
     * 用户修改成功
     */
    public function update($user_id, Request $request)
    {
        $acc = User::where("id", $user_id);
        $data = $request->all();
        if ($data['telphone']) {
            $user = User::where("telphone", $data['telphone'])->first();
            if ($user && $user['id'] != $user_id) {
                return $this->request_failed_json("The mobile number already exists");
            }
            $acc->update(['telphone' => $data['telphone']]);
        }
        if ($data['password']) {
            $acc->update(['password' =>  bcrypt($data['password'])]);
        }
        if ($data['name']) {
            $acc->update(['name' => $data['name']]);
        }
        if ($data['color']) {
            $acc->update(['color' => $data['color']]);
        }
        if ($data['status']) {
            $acc->update(['status' => $data['status']]);
        }
        return $this->request_success_json("User updated successfully");
    }

    /**
     * 用户修改密码
     */
    public function changePwd($user_id, Request $request)
    {
        $data = $request->all();
        User::where('id', $user_id)->update(['password' => bcrypt($data['password'])]);
        return $this->request_success_json("Password changed successfully");
    }

    /**
     * 用户列表
     */
    public function clients(Request $request)
    {
        $data = $request->all();
        $per_page = isset($data['limit']) ? $data['limit'] : 15;
        $clients = Clients::query();
        if ($data['status']) {
            $clients->where("status", $data['status']);
        }
        $clients = $clients->paginate($per_page);
        return $this->request_success_json($clients);
    }

    public function changeClients($client_id)
    {
        $client = Clients::where('id', $client_id);
        $status = 0;
        if ($client->value('status') == 1) {
            $status = 2;
        } else {
            $status = 1;
        }
        $client->update(['status' => $status]);
        return $this->request_success_json();
    }

    public function allUser()
    {
        $users =  User::select('id', 'name', 'color')->where('status', 1)->get();
        return $this->request_success_json($users);
    }
}
