<?php

namespace App\Http\Controllers\Utils;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UtilController extends Controller
{
    /**
     * 图片上传
     */
    public function uploadImg(Request $request)
    {
        $data = $request->all();
        $url = $_SERVER['HTTP_HOST'];
        $image = $data['file'];
        $path = 'images/';
        $image = $this->filesUpload($path, $image, '.png');
        // return $this->request_success_json('http://' . $url . '/' . $image['url']);
        return $this->request_success_json('/' . $image['url']);
    }

    public function uplodeVideo(Request $request){
        $data = $request->all();
        $file = $data['file'];
        $path = 'vedio/';
        $vedio = $this->filesUpload($path, $file, '.mp4');
        return $this->request_success_json('/' . $vedio['url']);
    }
}
