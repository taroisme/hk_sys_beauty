<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Http;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function toBase64($imgUrl)
    {
        $imageData = file_get_contents($imgUrl);
        $base64EncodedImage = base64_encode($imageData);
        return $base64EncodedImage;
    }

    /**
     * 成功-以json数据格式结果返回
     * @param $data
     * @return JsonResponse
     */
    public function request_success_json($data = '', $message = '')
    {
        $arr = array(
            'code' => 1,
            'message' => !empty($message) ? $message : '操作成功',
            'data' => $data
        );
        return new JsonResponse($arr);
    }

    /**
     *失败-以json数据格式返回错误的提示信息
     * @param $msg
     * @return JsonResponse
     */
    public function request_failed_json($msg = '操作失败')
    {
        $arr = array(
            'code' => 0,
            'message' => $msg
        );
        return new JsonResponse($arr);
    }
    /**
     * 文件上传
     * @param $url
     * @param $file
     * @param $fileName
     * @return array|bool
     */
    public function filesUpload($url, $file, $fileName)
    {
        $UPLOAD_DIR = $url;
        $name = $fileName;
        $start = strpos($file, ',');
        $file = substr($file, $start + 1);
        $file = str_replace(' ', '+', $file);
        $data = base64_decode($file);
        $startN = strripos($name, ".");
        $type = substr($name, $startN + 1);
        $url = $UPLOAD_DIR . uniqid() . "." . $type;
        if (!file_exists($UPLOAD_DIR)) {
            mkdir($UPLOAD_DIR, 0777, true);
            chmod($UPLOAD_DIR, 0777);
        }
        $success = file_put_contents($url, $data);
        if ($success) {
            $result = array(
                'url' => $url,
                'fileName' => $fileName,
                'type' => $type,
            );
            return $result;
        } else {
            return false;
        }
    }

    /**
     * 计算每月天数
     */
    public function days($datetime)
    {
        $arr = explode("-", $datetime);
        $year = $arr[0];
        $month = $arr[1];
        $isLeap = 0;
        if ($year % 4 == 0 && $year % 100 != 0) {
            $isLeap = 1;
        }
        if ($year % 400 == 0) {
            $isLeap = 1;
        }
        $day = 0;
        if ($isLeap == 1 && $month == 2) {
            $day = 29;
        }
        if ($isLeap == 0 && $month == 2) {
            $day = 28;
        }
        if (in_array($month, [1, 3, 5, 7, 8, 10, 12])) {
            $day = 31;
        }
        if (in_array($month, [4, 6, 9, 11])) {
            $day = 30;
        }
        return $day;
    }

    public function sendSms($p_number)
    {
        $randomCode = rand(100000, 999999);
        // $p_number = "60968522";
        $p_content = $this->Text2Unicode("你的驗證碼為：" . $randomCode . "，驗證碼有效期為5分鐘");
        $p_number = "852" . $p_number;
        $p_url = "https://www.meteorsis.com/misweb/f_sendsms.aspx?&langeng=0&dos=now&senderid=NOMOMOHK&username=NOMOMO&password=123456";
        $p_url .= "&content=" . $p_content;
        $p_url .= "&recipient=" . $p_number;
        $webres = file($p_url);
        $smsdid = explode(":", $webres[0]);
        return $randomCode;
        // if ($smsdid[0] == "SMSDID") {
        //     $v_url = "https://www.meteorsis.com/misweb/f_getsmsdstatus.aspx?&username=NOMOMO&password=123456";
        //     $v_url .= "&smsdid=" . $smsdid[1];
        //     $verWeb = file($v_url);
        //     $status = explode(";", $verWeb[0]);
            // if ($status[0] == "STATUS:2" || $status[0] == "STATUS:22") {
                // return $randomCode;
            // } else {
            //     return false;
            // }
        // } else {
        //     return false;
        // }
    }

    public function Text2Unicode($str)
    {
        $unicode = array();
        $values = array();
        $lookingFor = 1;
        for ($i = 0; $i < strlen($str); $i++) {
            $thisValue = ord($str[$i]);
            if ($thisValue < ord('A')) {
                if ($thisValue >= ord('0') && $thisValue <= ord('9')) {
                    $unicode[] = '00' . dechex($thisValue);
                } else {
                    $unicode[] = '00' . dechex($thisValue);
                }
            } else {
                if ($thisValue < 128)
                    $unicode[] = '00' . dechex($thisValue);
                else {
                    if (count($values) == 0) $lookingFor = ($thisValue < 224) ? 2 : 3;
                    $values[] = $thisValue;
                    if (count($values) == $lookingFor) {
                        $number = ($lookingFor == 3) ?
                            (($values[0] % 16) * 4096) + (($values[1] % 64) * 64) + ($values[2] % 64) : (($values[0] % 32) * 64) + ($values[1] % 64);
                        $number = dechex($number);
                        $unicode[] = (strlen($number) == 3) ? "0" . $number : "" . $number;
                        $values = array();
                        $lookingFor = 1;
                    }
                }
            }
        }
        for ($i = 0; $i < count($unicode); $i++) $unicode[$i] = str_pad($unicode[$i], 4, "0", STR_PAD_LEFT);
        return implode("", $unicode);
    }
}
