<?php

namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use App\Models\Images;
use App\Models\Modules;
use App\Models\ModuleType;
use Illuminate\Http\Request;

class ModulesController extends Controller
{

    /**
     * module type 列表
     */
    public function getType()
    {
        $mts = ModuleType::get();
        return $this->request_success_json($mts);
    }

    /**
     * 删除module type
     */
    public function delType($mt_id)
    {
        $isExist = Modules::where("mt_id", $mt_id)->exists();
        if ($isExist) {
            return $this->request_failed_json("The module is not empty,Type delete Failed");
        }
        ModuleType::where("id", $mt_id)->delete();
        return $this->request_failed_json("Type deleted successfully");
    }

    /**
     * 创建module type
     */
    public function createType(Request $request)
    {
        $data = $request->all();
        $type = $data['type'];
        $isExist = ModuleType::where('type', $type)->exists();
        if ($isExist) {
            return $this->request_failed_json("Type is exists");
        }
        ModuleType::create(['type' => $type]);
        return $this->request_success_json("Type created successfully");
    }

    /**
     * 修改mdoule type
     */
    public function editType($mt_id, Request $request)
    {
        $data = $request->all();
        $type = $data['type'];
        $moduleType = ModuleType::where("type", $type)->first();
        if ($moduleType && $mt_id != $moduleType['id']) {
            ModuleType::where('id', $mt_id)->update(['type' => $type]);
            return $this->request_success_json("Type updated successfully");
        } else {
            return $this->request_failed_json();
        }
    }
    /**
     * 模块列表
     */
    public function get(Request $request)
    {
        $data = $request->all();
        $per_page = isset($data['limit']) ? $data['limit'] : 15;
        $modules = Modules::query();
        if ($data['mt_id']) {
            $modules->where("mt_id", $data['mt_id']);
        }
        if ($data['name']) {
            $modules->where("name", 'LIKE', "%{$data['name']}%");
        }
        $modules = $modules->paginate($per_page);
        return $this->request_success_json($modules);
    }
    /**
     * 模块列表
     */
    public function detail($id)
    {
        $module = Modules::where('id', $id)->first();
        $module['images'] = Images::where('id', $id)->get();
        return $this->request_success_json($module);
    }

    /**
     * 新增module
     */
    public function create(Request $request)
    {
        $data = $request->all();
        $name = $data['name'];
        $mt_id = $data['mt_id'];
        $isExist = Modules::where('mt_id', $mt_id)->where("name", $name)->exists();
        if ($isExist) {
            return $this->request_failed_json("已存在");
        }
        Modules::create([
            "mt_id" => $mt_id,
            "name" => $name,
            "content" => $data['content']
        ]);

        return $this->request_success_json("新增成功");
    }

    /**
     * 修改module
     */
    public function update($id, Request $request)
    {
        $data = $request->all();
        $name = $data['name'];
        $module = Modules::where("name", $name)->first();
        if ($module && $id != $module['id']) {
            return $this->request_failed_json("已存在");
        }
        Modules::where("id", $id)
            ->update([
                "name" => $name,
                "content" => $data['content']
            ]);
        return $this->request_success_json("修改成功");
    }


    /**
     * 删除模块
     */
    public function delete($id)
    {
        Modules::where("id", $id)->delete();
        return $this->request_success_json("删除成功");
    }
}
