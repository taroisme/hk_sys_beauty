<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use App\Models\Programs;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
    public function get()
    {
        $programs = Programs::where("parent_id", 0)->where('status', 1)->get();
        foreach ($programs as $program) {
            $program["sub_menus"] = Programs::where("parent_id", $program['id'])->where('status', 1)->get();
        }
        return $this->request_success_json($programs);
    }
}
