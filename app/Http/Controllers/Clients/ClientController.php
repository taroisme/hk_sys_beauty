<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use App\Models\Clients;
use Facade\FlareClient\Http\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function update($client_id, Request $request)
    {
        $data = $request->all();
        $client = Clients::where('id', $client_id);
        if ($data['name']) {
            $client->update(['name' => $data['name']]);
        }
        if ($data['password']) {
            $client->update(['password' => bcrypt($data['password'])]);
        }
        return $this->request_success_json();
    }
}
