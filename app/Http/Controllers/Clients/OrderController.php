<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use App\Models\Clients;
use App\Models\OperationLog;
use App\Models\Orders;
use App\Models\Programs;
use App\Models\User;
use App\Models\WorkingArr;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * 可预约时间
     */
    public function dTimeList(Request $request)
    {
        $data = $request->all();
        $startAt = $data['datetime'] . " 09:00";
        $endAt = $data['datetime'] . " 23:00";
        $program_id = $data['program_id'];
        $arr = array();
        $was = WorkingArr::whereBetween('time', [strtotime($startAt), strtotime($endAt)])->where('program_id', $program_id)->orderBy('time', 'asc')->get();
        foreach ($was as $wa) {
            $isExist = Orders::where('time', $wa['time'])->where('program_id', $program_id)->where('status', 1)->exists();
            if (!$isExist) {
                $user = User::where('id', $wa['user_id'])->first();
                $pdj = array();
                $pdj['user_id'] = $user['id'];
                $pdj['userName'] = $user['name'];
                $pdj['color'] = $user['color'];
                $pdj['dateTime'] = date("Y-m-d H:i", $wa['time']);
                $arr[] = $pdj;
            }
        }
        return $this->request_success_json($arr);
    }


    public function mTimeList(Request $request)
    {
        $data = $request->all();
        $datetime = $data['datetime'];
        $program_id = $data['program_id'];
        $days = $this->days($datetime);
        $arr = array();
        for ($i = 1; $i <= $days; $i++) {
            $today = array();
            $today["day"] = $i;
            $startAt = strtotime($datetime . "-" . $i . " 09:00");
            $endAt = strtotime($datetime . "-" . $i . " 23:00");
            $was = WorkingArr::whereBetween("time", [$startAt, $endAt])->where('program_id', $program_id);
            $time = array();
            foreach ($was->get() as $wa) {
                $isExist = Orders::where('time', $wa['time'])->where('status', 1)->exists();
                if (!$isExist) {
                    $time[] = date("H:i", $wa['time']);
                }
            }
            $color = array();
            foreach ($was->select('user_id')->distinct()->get() as $wa) {
                $color[] = User::where('id', $wa['user_id'])->value('color');
            }
            if (count($time) > 0) {
                $today['color'] = $color;
                $today['time'] = $time;
                $arr[] = $today;
            }
        }
        return $this->request_success_json($arr);
    }
    /**
     * 顾客工单列表
     */
    public function get(Request $request)
    {
        $data = $request->all();
        $per_page = isset($data['limit']) ? $data['limit'] : 15;
        $client_id = $data['client_id'];
        $orders = Orders::where('client_id', $client_id);
        if ($data['status'] == 1) {
            $orders->where("status", 1);
        }
        if ($data['status'] != 1) {
            $orders->where("status", "<>", 1);
        }
        if ($data['program_id']) {
            $orders->where("program_id", $data['program_id']);
        }
        $orders = $orders->orderByDesc('id')->paginate($per_page);
        if (count($orders) > 0) {
            foreach ($orders as $order) {
                $order['time'] = date("Y-m-d H:i", $order['time']);
                $order['user_name'] = User::where("id", $order['user_id'])->value("name");
                $order['program'] = Programs::where('id', $order['program_id'])->value('name');
                $program_second = array();
                $ids = explode(",", $order['program_second_id']);
                foreach ($ids as $id) {
                    $program_second[] = Programs::where('id', $id)->value('name');
                }
                $order['program_second'] = $program_second;
            }
        }
        return $this->request_success_json($orders);
    }

    /**
     * 创建预约工单
     */
    public function create(Request $request)
    {
        $data = $request->all();
        $time = strtotime($data['time']);
        $program_id = $data['program_id'];
        $client_id = $data['client_id'];
        $client = Clients::where('id', $client_id);
        if ($client->value('status') == 2) {
            return $this->request_failed_json('您的帳戶已被凍結');
        }
        $timestamp = strtotime($time);
        $timeIsExist = Orders::where("time", $timestamp)->where("status", 1)->exists();
        if ($timeIsExist) {
            return $this->request_failed_json("時間已預約");
        }
        $isExist = Orders::where("client_id", $client_id)->where("program_id", $program_id)->where("status", 1)->exists();
        if ($isExist) {
            return $this->request_failed_json("用戶預約尚未完成或取消");
        }
        $user_id = WorkingArr::where('time', $time)->value('user_id');
        $order = Orders::create([
            "time" => $time,
            "program_id" => $program_id,
            "program_second_id" => $data['program_second_id'],
            "client_id" => $client_id,
            "user_id" => $user_id,
            "status" => 1,
            "remark" => $data['remark']
        ]);
        // if ($order) {
        //     OperationLog::create([
        //         "client_id" => $client_id,
        //         "order_id" => $order['id'],
        //         "operation" => "Create Order"
        //     ]);
        // }
        return $this->request_success_json("預約成功");
    }

    /**
     * 取消预约
     */
    public function cancel($order_id)
    {
        $timestamp = time();
        $order = Orders::where("id", $order_id);
        if ($order->value("time") - 259200 < $timestamp) {
            return $this->request_failed_json("Cancellations cannot be made within three days");
        }
        $order->update(['status' => 3]);
        // OperationLog::create([
        //     "client_id" => $order->value('client_id'),
        //     'order_id' => $order_id,
        //     'operation' => "cancel the reservation"
        // ]);
        return $this->request_failed_json("Cancel successfully");
    }

    /**
     * 修改预约
     */
    public function update($order_id, Request $request)
    {
        $data = $request->all();
        $timestamp = time();
        $order = Orders::where("id", $order_id);
        if ($order->value("time") - 259200 < $timestamp) {
            return $this->request_failed_json("Reservation time cannot be changed within three days");
        }
        $time = strtotime($data['time']);
        $user_id = WorkingArr::where('time', $time)->value('user_id');
        $order->update([
            "remark" => $data['remark'],
            'program_id' => $data['program_id'],
            'time' => $time,
            'user_id' => $user_id,
            "program_second_id" => $data['program_second_id'],
        ]);
        // OperationLog::create([
        //     "client_id" => $order->value('client_id'),
        //     'order_id' => $order_id,
        //     'operation' => "Modify reservation time"
        // ]);
        return $this->request_success_json("Update succeeded");
    }
}
