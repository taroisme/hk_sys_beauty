<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use App\Models\Clients;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{

    /**
     * 发送验证码
     */
    public function registerSendCode(Request $request)
    {
        $data = $request->all();
        $tel = $data['telphone'];
        $code = $this->sendSms($tel);
        if ($code == false) {
            return $this->request_failed_json('發送失敗');
        }
        $timestamp = time();
        $client = Clients::where('telphone', $tel);
        if ($client->exists()) {
            $time = $client->value('dateTime');
            if ($timestamp - $time < 60) {
                return $this->request_failed_json($timestamp - $time);
            }
            $client->update([
                'verCode' => $code,
                'dateTime' => $timestamp
            ]);
        } else {
            Clients::create([
                'telphone' => $tel,
                'verCode' => $code,
                'dateTime' => $timestamp
            ]);
        }

        return $this->request_success_json($code);
    }

    /**
     * 重置密码验证码发送
     */
    public function rePwdSendCode(Request $request)
    {
        $data = $request->all();
        $tel = $data['telphone'];
        $timestamp = time();
        $client = Clients::where('telphone', $tel)->where('status', 1);
        if ($client->exists()) {
            $time = $client->value('dateTime');
            if ($timestamp - $time < 60) {
                return $this->request_failed_json($timestamp - $time);
            }
            $code = $this->sendSms($tel);
            if ($code == false) {
                return $this->request_failed_json('發送失敗');
            }
            $client->update([
                'verCode' => $code,
                'dateTime' => $timestamp
            ]);
            return $this->request_success_json('發送成功');
        } else {
            return $this->request_failed_json('手機號不存在');
        }
    }


    /**
     * 用户验证
     */
    public function register(Request $request)
    {
        $data = $request->all();
        $timestamp = time();
        $tel = $data['telphone'];
        $code = $data['verCode'];
        $client = Clients::where("telphone", $tel);
        if ($code != $client->value("verCode")) {
            return $this->request_failed_json("驗證碼錯誤");
        }
        if ($client->value("dateTime") + 300 < $timestamp) {
            return $this->request_failed_json("驗證碼超時");
        }
        $arr = array(
            "verCode" => null,
            "dateTime" => null,
            "password" => bcrypt($data['password']),
            "name" => $data['name'],
            "email" => $data['email'],
            "status" => 1
        );
        $client->update($arr);
        return $this->request_success_json();
    }

    public function rePwd(Request $request)
    {
        $data = $request->all();
        $timestamp = time();
        $tel = $data['telphone'];
        $code = $data['verCode'];
        $client = Clients::where("telphone", $tel);
        if ($code != $client->value("verCode")) {
            return $this->request_failed_json("驗證碼錯誤");
        }
        if ($client->value("dateTime") + 300 < $timestamp) {
            return $this->request_failed_json("驗證碼超時");
        }
        $arr = array(
            "verCode" => null,
            "dateTime" => null,
            "password" => bcrypt($data['password']),
        );
        $client->update($arr);
        return $this->request_success_json();
    }
    /**
     * 登录
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function login(Request $request)
    {
        // $this->validateLogin($request);
        $data = $request->all();
        $telphone = $data['telphone'];
        $password = $data['password'];
        if ($telphone == null || $password == null) {
            return $this->request_failed_json('賬號或者密碼不允許為空');
        }
        $client = Clients::where('telphone', $telphone);
        if (!$client->exists()) {
            return $this->request_failed_json('賬號不存在');
        }
        if ($client->value('status') == 2) {
            return $this->request_failed_json('賬戶被凍結');
        }
        if (Hash::check($password, $client->value('password'))) {
            // $app_token = $this->generateToken();
            $acc = Clients::where('telphone', $telphone)->first();
            return $this->request_success_json($acc);
        } else {
            return $this->request_failed_json('密碼錯誤');
        }
    }
}
