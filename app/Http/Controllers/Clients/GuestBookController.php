<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use App\Models\GuestBook;
use Illuminate\Http\Request;

class GuestBookController extends Controller
{
    public function create(Request $request){
        $data = $request->all();
        GuestBook::create([
            'name'=>$data['name'],
            'email'=>$data['email'],
            'phone'=>$data['phone'],
            'content'=>$data['content'],
        ]);
        return $this->request_success_json("創建成功");
    }
}
