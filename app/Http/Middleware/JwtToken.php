<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class JwtToken
{
    public function handle(Request $request, Closure $next)
    {
        $token = $request->bearerToken();
        if (!$token) {
            return response()->json(['error' => 'Token not provided'], 401);
        }
        try {
            $user = JWTAuth::toUser($token);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Token is invalid'], 401);
        }
        $request->merge(['user' => $user]);
        return $next($request);
    }
}
