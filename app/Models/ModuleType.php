<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModuleType extends Model
{
    protected $table = 'module_type';

    public $timestamps = false;

    protected $guarded = ['id'];
}