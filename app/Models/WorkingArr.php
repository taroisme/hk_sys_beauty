<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkingArr extends Model
{
    /**
     * 工作安排
     */
    protected $table = 'workingarr';

    public $timestamps = false;

    protected $guarded = ['id'];
}
