<?php

use App\Http\Controllers\Admin\GuestBookController as AdminGuestBookController;
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\Admin\OrderController as AdminOrderController;
use App\Http\Controllers\Admin\ProgramController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Clients\ClientController;
use App\Http\Controllers\Clients\GuestBookController;
use App\Http\Controllers\Clients\LoginController as ClientsLoginController;
use App\Http\Controllers\Clients\OrderController;
use App\Http\Controllers\Utils\UtilController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Clients\ProgramController as ClientsProgramController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Modules\ModulesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('upload/img', [UtilController::class, 'uploadImg']);
Route::post('upload/video', [UtilController::class, 'uplodeVideo']);


Route::group(['namespace' => 'Clients', 'prefix' => 'client'], function () {
    Route::post('/login', [ClientsLoginController::class, 'login']); //登录
    Route::post('/register/code', [ClientsLoginController::class, 'registerSendCode']);
    Route::put('/repwd/code', [ClientsLoginController::class, 'rePwdSendCode']);
    Route::put('/repwd', [ClientsLoginController::class, 'rePwd']);
    Route::put('/register', [ClientsLoginController::class, 'register']);

    /**
     * order
     */
    Route::get('/order', [OrderController::class, 'get']);
    Route::get('/day/time/list', [OrderController::class, 'dTimeList']);
    Route::get('/month/time/list', [OrderController::class, 'mTimeList']);
    Route::get('/order/time/list', [OrderController::class, 'timeList']);
    Route::post('/order', [OrderController::class, 'create']);
    Route::put('/order/cancel/{order_id}', [OrderController::class, 'cancel']);
    Route::put('/order/{order_id}', [OrderController::class, 'update']);
    /**
     * clients
     */
    Route::put('/{client_id}', [ClientController::class, 'update']);
    Route::get('/program', [ClientsProgramController::class, 'get']);
    /**
     * guest book
     */
    Route::post('/guestbook', [GuestBookController::class, 'create']);
});
Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::post('/login', [LoginController::class, 'login']); //登录
    Route::get('/user', [UserController::class, 'get']);
    Route::post('/user', [UserController::class, 'create']);
    Route::put('/user/{user_id}', [UserController::class, 'update']);
    Route::put("/change/password/{user_id}", [UserController::class, 'changePwd']);
    Route::get("/clients", [UserController::class, 'clients']);
    Route::put("/clients/{client_id}", [UserController::class, 'changeClients']);
    /**
     * program
     */
    Route::get('/program', [ProgramController::class, 'get']);
    Route::get('/month/time/list', [ProgramController::class, 'mTimeList']);
    Route::get('/day/time/list', [ProgramController::class, 'dTimeList']);
    Route::post('/program', [ProgramController::class, 'create']);
    Route::put('/program/{program_id}', [ProgramController::class, 'update']);

    Route::post('/working/arr', [ProgramController::class, 'addWorkingArr']);
    Route::delete('/working/arr/{wa_id}', [ProgramController::class, 'delWorkingArr']);
    /**
     * Order
     */
    Route::get("/orders", [AdminOrderController::class, "get"]);
    Route::put("/orders/{order_id}", [AdminOrderController::class, "update"]);

    /**
     * guest book
     */
    Route::get('/guestbook', [AdminGuestBookController::class, 'get']);
    Route::put('/guestbook/{gb_id}', [AdminGuestBookController::class, 'update']);
    Route::delete('/guestbook/{gb_id}', [AdminGuestBookController::class, 'delete']);

    Route::get('/users', [UserController::class, 'allUser']);
});

Route::group(['namespace' => 'Modules', 'prefix' => 'module'], function () {
    Route::get("", [ModulesController::class, "get"]);
    Route::get("/{module_id}", [ModulesController::class, "detail"]);
    Route::post("", [ModulesController::class, "create"]);
    Route::put("/{module_id}", [ModulesController::class, "update"]);
    Route::delete("/{module_id}", [ModulesController::class, "delete"]);


    Route::get("/type", [ModulesController::class, "getType"]);
    Route::delete("/type/{mt_id}", [ModulesController::class, "delType"]);
    Route::put("/type/{mt_id}", [ModulesController::class, "editType"]);
    Route::post("/type", [ModulesController::class, "createType"]);
});