<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string("telphone")->unique();
            $table->string('password');
            $table->string('name');
            $table->integer("level")->comment("等级")->nullable();
            $table->integer("integral")->comment("积分")->nullable();
            $table->integer("verCode")->comment("验证码")->nullable();
            $table->string("dateTime")->nullable();
            $table->integer("status")->default(1)->comment("1:白名单;2:黑名单");
            $table->string("email");
            $table->integer("isNewClient")->default(1)->comment("1:新顾客;2:老顾客");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
