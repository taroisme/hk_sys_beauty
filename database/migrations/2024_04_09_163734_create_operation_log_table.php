<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOperationLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operationLogs', function (Blueprint $table) {
            $table->id();
            $table->integer("client_id")->nullable();
            $table->integer("user_id")->nullable();
            $table->integer("order_id");
            $table->string("operation");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operationLogs');
    }
}
