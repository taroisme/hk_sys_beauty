<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string("time")->nullable();
            $table->integer("program_id")->nullable();
            $table->string("program_second_id")->nullable();
            $table->integer("user_id");
            $table->integer("client_id");
            $table->integer("status")->default(1)->comment("1：已预约；2；已完成；3：已取消；4：已过期");
            $table->string('remark')->comment("用户备注")->nullable();
            $table->string('remarks')->comment("管理员备注")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
