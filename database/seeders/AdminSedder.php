<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class AdminSedder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'account' => 'admin',
            'password' => bcrypt("123456"),
            'name' => 'administration',
            'role' => 0
        ]);
    }
}
